package com.lirancohen.designtoolbox_referenced_pager_adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

/**
 * Created by Liran on 1/4/2016.
 *
 * A specialized FragmentStatePagerAdapter which keep track of its child fragments and may
 * return the instance of a selected fragment upon request
 */
public class ReferencedPagerAdapter extends FragmentStatePagerAdapter {

    SparseArray<Fragment> registeredFragments = new SparseArray<>();

    public ReferencedPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    /** this method will always return null, use getFragmentAt instead */
    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    /** return the child fragment at specified position or null if child is not yet
     * created
     */
    public Fragment getFragmentAt(int position){
        return registeredFragments.get(position);
    }

    /** return the index of the dataset co-responding the to the specified child fragment
     * or -1 if fragment is not part of the adapter
     */
    public int getIndexOfFragment(Fragment fragment){
        if(fragment != null) {
            for (int i = 0; i < registeredFragments.size(); i++) {
                int key = registeredFragments.keyAt(i);
                Fragment sparseFragment = registeredFragments.get(key);
                if (sparseFragment != null && sparseFragment == fragment) return key;
            }
        }
        return -1;
    }
}
