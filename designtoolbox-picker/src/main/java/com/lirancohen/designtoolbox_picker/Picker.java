package com.lirancohen.designtoolbox_picker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Liran on 1/28/2016.
 *
 * A widget used to display a pre-defined set of values and cycle through using either scrolling
 * or buttons. This widget is an easy to style version of the default NumberPicker.
 * Picker class can be used in three ways:
 * <p>
 * 1) using the default adapter by calling <i>setDefaultAdapter(List)</i>, this will display supplied items as strings.
 * 2) specifying a custom adapter by calling <i>setAdapter(PickerAdapter)</i>, this allow displaying arbitrary data sets<br>
 * 3) subclassing the Picker class and overriding the <i>getDefaultAdapter()</i> method to return a custom adapter, this allow
 *    to create a quick type of picker which doesn't require specifying special adapter for each implementation.
 * </p>
 *
 * Additional customization can be done by overriding the <i>applyItemTransformation()</i> method to create custom transition effect
 * based on the distance of the item in the list from the selected item position.<br><br>
 *
 * When extending the Picker to create a custom widget it is recommended to override <i>getExtendingAttributes()</i> to fetch
 * additional attributes in the appropriate lifecycle stage<br><br>
 *
 * <b>Note:</b> please read instructions of using PickerAdapter before creating custom version of the adapter
 */
public class Picker extends LinearLayout {
    private static final String TAG = "Picker";

    private static final String PREV_BUTTON_TAG = "widget.picker:prevButton";
    private static final String NEXT_BUTTON_TAG = "widget.picker:nextButton";

    private static final int ITEMS_FOR_DEFAULT_WRAP_AROUND = 15;
    private static final int DEFAULT_DISPLAY_SIZE = 100; //dp
    private static final boolean DEFAULT_SCROLLABLE = true;
    private static final boolean DEFAULT_SHOW_BUTTONS = true;
    private static final float DEFAULT_TEXT_SIZE = 20; // sp
    private static final int DEFAULT_TEXT_COLOR = Color.BLACK;
    private static final int DEFAULT_BUTTONS_COLOR = Color.BLACK;
    private static final int DEFAULT_BUTTONS_LARGE_DIMENSION = 50; //dp
    private static final int DEFAULT_BUTTONS_SMALL_DIMENSION = 22; //dp
    private static final int DEFAULT_BUTTONS_MARGIN = 3; //dp

    //Settable properties
    private int prevButtonRes = -1;
    private int nextButtonRes = -1;
    private boolean showButtons = DEFAULT_SHOW_BUTTONS;
    private boolean wrapAround;
    private boolean scrollable = DEFAULT_SCROLLABLE;
    private int displaySize = DEFAULT_DISPLAY_SIZE;
    private float textSize = DEFAULT_TEXT_SIZE;
    private int textColor = DEFAULT_TEXT_COLOR;
    private int buttonsColor = DEFAULT_BUTTONS_COLOR;
    private int divider = -1;

    //internal properties
    WeakReference<View> footerWeakRef;
    WeakReference<View> headerWeakRef;

    private int selectedItem = 0;
    private int previouslySelectedItem = 0;
    private int currentBestCandidateForSelection = 0;
    private int childSize = 0;
    private int headerFooterSize = 0;
    private boolean scrollingByUser = false;
    private boolean isSetWrapAround = false;
    private boolean adjustingToPosition = false;
    private boolean hasHeaderFooter = false;

    private OnSelectionChangeListener callbacks;

    private enum BUTTON{
        PREV, NEXT
    }

    public interface OnSelectionChangeListener{
        /** called when the currently selected item was changed
         *
         * @param newSelection the position of the new selection in the data-set
         * @param oldSelection the position of the previous selection in the data-set
         * @param isFinal If the selection has settled or is the picker still moving
         */
        void onSelectionChanged(int newSelection, int oldSelection, boolean isFinal);
    }

    public Picker(Context context) {
        super(context);
        initPicker(context, null);
    }

    public Picker(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPicker(context, attrs);
    }

    public Picker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPicker(context, attrs);
    }

    private void getPickerAttributes(Context context, AttributeSet attrs) {
        if(context == null ||attrs == null) return;

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.Picker);

        if(attributes == null) return;

        prevButtonRes = attributes.getResourceId(R.styleable.Picker_prevButton, -1);
        nextButtonRes = attributes.getResourceId(R.styleable.Picker_nextButton, -1);
        displaySize = (int) attributes.getDimension(R.styleable.Picker_displaySize, context.getResources().getDisplayMetrics().density * DEFAULT_DISPLAY_SIZE);
        scrollable = attributes.getBoolean(R.styleable.Picker_scrollable, DEFAULT_SCROLLABLE);
        showButtons = attributes.getBoolean(R.styleable.Picker_showButtons, DEFAULT_SHOW_BUTTONS);
        textColor = attributes.getColor(R.styleable.Picker_textColor, DEFAULT_TEXT_COLOR);
        textSize = (attributes.hasValue(R.styleable.Picker_textSize)) ?
                (attributes.getDimension(R.styleable.Picker_textSize, DEFAULT_TEXT_SIZE) / context.getResources().getDisplayMetrics().scaledDensity)
                : DEFAULT_TEXT_SIZE;
        buttonsColor = attributes.getColor(R.styleable.Picker_buttonsColor, DEFAULT_BUTTONS_COLOR);
        if(attributes.hasValue(R.styleable.Picker_wrapAround)) {
            isSetWrapAround = true;
            wrapAround = attributes.getBoolean(R.styleable.Picker_wrapAround, false);
        }
        divider = attributes.getResourceId(R.styleable.Picker_itemDivider, -1);

        attributes.recycle();

        TypedArray viewAttributes = context.obtainStyledAttributes(attrs, new int[]{android.R.attr.layout_height});
        if(viewAttributes.getInt(0, ViewGroup.LayoutParams.WRAP_CONTENT) == ViewGroup.LayoutParams.MATCH_PARENT){
            displaySize = 0;
        }
        viewAttributes.recycle();
    }

    /** This method used to supply easy implementation of custom xml attributes, default implementation
     * does nothing, you should call recycle on the AttributeSet when finished processing the custom attributes
     */
    protected void getExtendingAttributes(Context context, AttributeSet attrs){
        //Do nothing, used by extending classes
    }

    private void initPicker(Context context, AttributeSet attrs){
        if(getChildCount() > 0) throw new IllegalStateException("Picker cannot host child views, please fix xml definition ["+this.getId()+"]");

        setOrientation(VERTICAL); //TODO make widget support both orientations using recycleView instead of listView

        getPickerAttributes(context, attrs);
        getExtendingAttributes(context, attrs);

        setPrevButton(prevButtonRes);
        setListView();
        setNextButton(nextButtonRes);
        setShowButtons(showButtons);
        setScrollable(scrollable);
    }

    /** create the list used to display the values of the picker */
    private void setListView() {
        if(getLayoutParams() != null && getLayoutParams().height == ViewGroup.LayoutParams.MATCH_PARENT){
            displaySize = 0;
        }

        LayoutParams params = getOrientation() == VERTICAL ?
                new LayoutParams(LayoutParams.MATCH_PARENT, displaySize) :
                new LayoutParams(displaySize, LayoutParams.MATCH_PARENT);

        if(displaySize <= 0){
            params.weight = 1;
        }

        if(this.getChildCount() > 1){
            this.getChildAt(1).setLayoutParams(params);
        } else {
            ListView listView = new ListView(getContext());
            listView.setLayoutParams(params);
            listView.setOnTouchListener(touchListener);
            //set divider
            if(divider == -1){
                listView.setDivider(null);
            } else if(Build.VERSION.SDK_INT >= 21) {
                listView.setDivider(getResources().getDrawable(divider, null));
            } else {
                listView.setDivider(getResources().getDrawable(divider));
            }
            this.addView(listView, 1);
        }
    }

    /** Set a layout resource to be used as the previous item button for the picker
     *
     * @param buttonRes A view resource to be used as the previous button.
     */
    public void setPrevButton(int buttonRes) {
        this.prevButtonRes = buttonRes;

        View button = buttonRes == -1 ? getDefaultButton(BUTTON.PREV) : LayoutInflater.from(getContext()).inflate(buttonRes, this, false);

        if(this.getChildCount() > 0){
            this.removeView(getChildAt(0));
        }

        this.addView(button, 0);

        button.setOnClickListener(clickListener);
        button.setTag(PREV_BUTTON_TAG);
    }

    /** return the color of the prev/next buttons (only applied if not using custom resource) */
    public int getButtonsColor() {
        return buttonsColor;
    }

    /** set the color of the prev/next buttons (only applied if not using custom resource) */
    public void setButtonsColor(int color) {
        this.buttonsColor = color;
    }

    /** set the color of the prev/next buttons (only applied if not using custom resource) */
    public void setButtonsColor(String colorString) {
        setButtonsColor(Color.parseColor(colorString));
    }

    /** Set a layout resource to be used as the next button for the picker
     *
     * @param buttonRes A view resource to be used as the next button.
     */
    public void setNextButton(int buttonRes) {
        this.nextButtonRes = buttonRes;

        View button = buttonRes == -1 ? getDefaultButton(BUTTON.NEXT) : LayoutInflater.from(getContext()).inflate(buttonRes, this, false);

        if(this.getChildCount() > 2){
            this.removeView(getChildAt(2));
        }

        this.addView(button, 2);

        button.setOnClickListener(clickListener);
        button.setTag(NEXT_BUTTON_TAG);
    }

    private OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {

            if(getData() == null) return;

            Object tagObject = v.getTag();
            if(tagObject instanceof String){
                adjustingToPosition = true;
                String tag = (String) tagObject;
                if(PREV_BUTTON_TAG.equals(tag)){
                    previouslySelectedItem = selectedItem;
                    if(selectedItem > 0) setSelectedItemInternal(selectedItem - 1, true);
                } else if(NEXT_BUTTON_TAG.equals(tag)){
                    previouslySelectedItem = selectedItem;
                    if(wrapAround){
                        if(selectedItem < PickerAdapter.MAX_VALUE - 1) setSelectedItemInternal(selectedItem + 1, true);
                    } else {
                        if(selectedItem < getData().size() - 1) setSelectedItemInternal(selectedItem + 1, true);
                    }
                } else {
                    Log.d(TAG,"Picker onClick was called with an unknown tag:"+tag);
                }
            }
        }
    };

    /** set the size of the scrollable display in px */
    public void setDisplaySize(int displaySize) {
        this.displaySize = displaySize;

        setListView();
    }

    /** return the size of the scrollable display in px */
    public int getDisplaySize() {
        return displaySize;
    }

    /** Set whether or not the increment and decrement buttons should be displayed or only rely
     * on scrolling for selection */
    public void setShowButtons(boolean showButtons) {
        this.showButtons = showButtons;

        if(getChildCount() > 0) getChildAt(0).setVisibility(showButtons ? View.VISIBLE : View.GONE);
        if(getChildCount() > 2) getChildAt(2).setVisibility(showButtons ? View.VISIBLE : View.GONE);

        if(!scrollable && !showButtons) Log.w(TAG, "Picker was set as both un-scrollable and button-less, selection cannot be changed using normal interaction:["+this.getId()+"]");
    }

    /** Return whether or not the increment and decrement buttons should be displayed or only rely
     * on scrolling for selection */
    public boolean isShowButtons() {
        return showButtons;
    }

    /** Set whether or not the display should be scrollable or only rely on buttons for selection */
    public void setScrollable(boolean scrollable) {
        this.scrollable = scrollable;

        if(getChildCount()>1) getChildAt(1).setEnabled(scrollable);

        if(!scrollable && !showButtons) Log.w(TAG, "Picker was set as both un-scrollable and button-less, selection cannot be changed using normal interaction:["+this.getId()+"]");
    }

    /** Return whether or not the display should be scrollable or only rely on buttons for selection */
    public boolean isScrollable() {
        return scrollable;
    }

    /** return the default adapter used to handle data-set presentation in the picker display.
     * Default implementation return an array adapter which convert the items to the items strings and
     * display the items in a simple TextView
     *
     * This method can be overridden by extending classes to create a picker which support a custom adapter
     * by default
     */
    protected PickerAdapter getDefaultAdapter(List data){
        return new PickerAdapter<Object>(getContext(), -1, data){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(!(convertView instanceof LinearLayout)){
                    convertView = new LinearLayout(getContext());
                    AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    ((LinearLayout) convertView).setGravity(Gravity.CENTER);
                    convertView.setLayoutParams(params);

                    TextView tv = new TextView(getContext());
                    tv.setTextSize(textSize);
                    tv.setTextColor(textColor);
                    tv.setGravity(Gravity.CENTER);
                    ((LinearLayout) convertView).addView(tv);
                }
                try {
                    ((TextView) ((LinearLayout) convertView).getChildAt(0)).setText(getItem(position).toString());
                } catch (Exception e){}

                return convertView;
            }
        };
    }

    /** return the adapter currently in use by this picker to display the data-set */
    public PickerAdapter getAdapter(){
        if(getChildCount() > 1){
            ListView listView = (ListView) getChildAt(1);
            ListAdapter adapter = listView.getAdapter();
            if(adapter instanceof HeaderViewListAdapter){
                adapter = ((HeaderViewListAdapter) adapter).getWrappedAdapter();
            }

            return (PickerAdapter) adapter;
        }
        return null;
    }

    /** Set the the currently selected item position, this is the same as calling setSelectedItem(int, false) */
    public void setSelectedItem(int selectedItem) {
        setSelectedItem(selectedItem, false);
    }

    /** Set the currently selected item position with animation
     *
     * @param selectedItem the position of the selected item
     * @param animate whether or not should smooth scroll to the selected position
     */
    public void setSelectedItem(int selectedItem, boolean animate){
        setSelectedItemInternal(selectedItem, animate);
    }

    /** an internal call of setSelectedItem to allow extending classes to override setSelectedItem
     * without interfering with core functionality
     */
    private void setSelectedItemInternal(int selectedItem, boolean animate){
        if(!animate) previouslySelectedItem = this.selectedItem;
        this.selectedItem = getClosestWrappedPosition(previouslySelectedItem ,selectedItem);
        scrollingByUser = false;
        if(getChildCount() > 1) {
            ListView listView = (ListView) getChildAt(1);
            if(animate){
                smoothScrollToPositionFromTop(listView, hasHeaderFooter ? this.selectedItem + 1 : this.selectedItem, getItemPositionFromTop());
            } else {
                listView.setSelectionFromTop(hasHeaderFooter ? this.selectedItem + 1 : this.selectedItem, getItemPositionFromTop());
            }
        }

        if(callbacks != null) callbacks.onSelectionChanged(getSelectedItem(), fixPosition(getUnwrappedPosition(previouslySelectedItem)), true);
    }

    /** Return the position of the currently selected item */
    public int getSelectedItem() {
        return fixPosition(getUnwrappedPosition(selectedItem));
    }

    /** Return whether or not the display loop the items in the data-set or not */
    public boolean isWrapAround() {
        return wrapAround;
    }

    /** set whether or not the display should loop the items in the data-set or not, this is set at
     * adapter level */
    public void setWrapAround(boolean wrapAround) {
        this.wrapAround = wrapAround;
        this.isSetWrapAround = true;

        if(getChildCount() >1){
            ListView listView = (ListView) getChildAt(1);
            PickerAdapter adapter = (listView.getAdapter() instanceof HeaderViewListAdapter) ?
                    (PickerAdapter) ((HeaderViewListAdapter) listView.getAdapter()).getWrappedAdapter() :
                    (PickerAdapter) listView.getAdapter();

            if(adapter != null){
                adapter.setWrapAround(wrapAround);
                if(wrapAround) positionPickerInMiddle(adapter);
            }
        }
    }

    /** Set wrapAround property to its default value and handling */
    public void setWrapAroundDefault(){
        if(getData() != null){
            wrapAround = getData().size() >= ITEMS_FOR_DEFAULT_WRAP_AROUND;
        }
        this.isSetWrapAround = false;

        if(getChildCount() >1){
            ListAdapter adapter = ((ListView) getChildAt(1)).getAdapter();
            if(adapter instanceof HeaderViewListAdapter) {
                adapter = ((HeaderViewListAdapter) adapter).getWrappedAdapter();
            }

            if (adapter != null) ((PickerAdapter) adapter).setWrapAround(wrapAround);
        }
    }

    public void setDefaultAdapter(List<?> data){
        buildListView(getDefaultAdapter(data));
    }

    /** set the data-set displayed by this picker using the specified adapter. to use custom item view
     * for complex type array either subclass the Picker class and override <i>getDefaultAdapter</i>
     * or call <i>setAdapter(PickerAdapter adapter)</i>
     *
     * @param adapter an adapter used to properly display the data-set
     */
    public void setAdapter(PickerAdapter adapter){
        buildListView(adapter);
    }

    /** get the data-set currently represented by this picker */
    public List getData() {
        return (getAdapter() != null) ? getAdapter().getData() : null;
    }

    /** set a listener to notify selection changes with */
    public void setOnSelectionChangeListener(OnSelectionChangeListener listener){
        this.callbacks = listener;
    }

    private void buildListView(PickerAdapter adapter){
        if(this.getChildCount() < 2) return;

        final ListView listView = (ListView) getChildAt(1);
        listView.setHorizontalScrollBarEnabled(false);
        listView.setVerticalScrollBarEnabled(false);
        listView.setAdapter(null);

        if(headerWeakRef == null) {
            View header = new View(getContext());
            headerWeakRef = new WeakReference<View>(header);
            listView.addHeaderView(header);
            hasHeaderFooter = true;
        }

        if(footerWeakRef == null) {
            View footer = new View(getContext());
            footerWeakRef = new WeakReference<View>(footer);
            listView.addFooterView(footer);
            hasHeaderFooter = true;
        }

        if(adapter == null || adapter.getData() == null || adapter.getData().size() == 0){
            return;
        } else if(!isSetWrapAround){
            wrapAround = adapter.getData().size() >= ITEMS_FOR_DEFAULT_WRAP_AROUND;
        }

        adapter.setWrapAround(wrapAround);
        listView.setAdapter(adapter);

        if(wrapAround) positionPickerInMiddle(adapter);
    }

    /** return the size of the item's text in sp, this only applied if using the default adapter */
    public float getTextSize() {
        return textSize;
    }

    /** set the size of the item's text in sp, this only applied if using the default adapter */
    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    /** set the color of the item's text, this only applied if using the default adapter */
    public int getTextColor() {
        return textColor;
    }

    /** set the color of the item's text, this only applied if using the default adapter */
    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        View child = ((ListView)getChildAt(1)).getChildAt(1);
        if(childSize == 0 && child != null){
            getChildAt(1).getViewTreeObserver().addOnGlobalLayoutListener(
                    new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            if(Build.VERSION.SDK_INT >= 16){
                                getChildAt(1).getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            } else {
                                getChildAt(1).getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            }
                            onAdapterReady((ListView)getChildAt(1) ,((ListView) getChildAt(1)).getChildAt(1));
                        }
                    });
        }
    }

    /** called when the adapter has been set and at least one item has been inflated, this
     * method will set the ListView header and footer dimensions to so the first and last items
     * can be centered in the display area of the picker. This method than also request the picker
     * to set its display on the currently selected item position*/
    private void onAdapterReady(ListView listView, View child){
        if(child == null || listView == null) return;
        child.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        childSize = getOrientation() == VERTICAL ?
                child.getHeight() : child.getWidth();

        if(childSize == 0) return;

        headerFooterSize = Math.max(0, (displaySize - childSize)/2);

        AbsListView.LayoutParams params = getOrientation() == VERTICAL ?
                new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, headerFooterSize) :
                new AbsListView.LayoutParams(headerFooterSize, AbsListView.LayoutParams.MATCH_PARENT);

        View header = headerWeakRef.get();

        if(header != null){
            if(childSize >= displaySize){
                listView.removeHeaderView(header);
                hasHeaderFooter = false;
            } else {
                header.setLayoutParams(params);
            }
        }

        View footer = footerWeakRef.get();
        if(header != null){
            if(childSize >= displaySize){
                listView.removeFooterView(footer);
                hasHeaderFooter = false;
            } else {
                footer.setLayoutParams(params);
            }
        }

        setSelectedItemInternal(selectedItem, false);
        listView.setOnScrollListener(scrollListener);
    }

    /** Return the position an item should be relative to the top of the display in order for the
     * item to appear at the center of the view
     */
    private int getItemPositionFromTop(){
        return headerFooterSize > 0 ? headerFooterSize : (displaySize - childSize)/2;
    }

    private OnTouchListener touchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getAction();
            switch (action){
                case MotionEvent.ACTION_UP:
                    scrollingByUser = true;
                    break;
                case MotionEvent.ACTION_DOWN:
                    scrollingByUser = false;
                    adjustingToPosition = false;
                    break;
            }
            return false;
        }
    };

    /** A scroll listener used to position items correctly after touch was lifted */
    private ListView.OnScrollListener scrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if(scrollState == SCROLL_STATE_IDLE){
                adjustingToPosition = false;
                if(scrollingByUser) {
                    scrollingByUser = false;
                    adjustToPosition();
                }
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            int bestCandidateForSelection = currentBestCandidateForSelection;
            float bestCandidateForSelectionDistance = 1;

            for(int i=0; i<visibleItemCount; i++){
                View child = view.getChildAt(i);
                float relativeDistanceFromSelection = getRelativeDistanceFromSelection(child);

                if(relativeDistanceFromSelection < bestCandidateForSelectionDistance){
                    if(firstVisibleItem == 0 && hasHeaderFooter){
                        //header is showing (it is child at 0, need to correct)
                        if(i==0){
                            //child is the header, skip it
                            continue;
                        } else {
                            //child is not the header but its visible so reported index should be reduced by 1
                            bestCandidateForSelection = fixFirstVisiblePosition(firstVisibleItem) + i - 1;
                        }
                    } else {
                        //header not showing, allow normal processing
                        bestCandidateForSelection = fixFirstVisiblePosition(firstVisibleItem) + i;
                    }
                    bestCandidateForSelectionDistance = relativeDistanceFromSelection;
                }
                applyItemTransformation(child, relativeDistanceFromSelection);
            }
            if(getData() != null
                    && selectedItem != bestCandidateForSelection
                    && !adjustingToPosition
                    && bestCandidateForSelection != currentBestCandidateForSelection) {
                previouslySelectedItem = currentBestCandidateForSelection;
                currentBestCandidateForSelection = bestCandidateForSelection;
                if(callbacks != null) callbacks.onSelectionChanged(
                        fixPosition(getUnwrappedPosition(bestCandidateForSelection)),
                        fixPosition(getUnwrappedPosition(previouslySelectedItem)),
                        false);
            }
        }
    };

    /** select the item which view is closest to the center of the display area */
    private void adjustToPosition(){
        adjustingToPosition = true;
        int childAtCenter = 0;
        ListView view = (ListView) getChildAt(1);

        for(int i=0 ,bestDistanceFromCenter = Integer.MAX_VALUE; i< view.getChildCount(); i++){
            View child = view.getChildAt(i);

            int center = getOrientation() == VERTICAL ?
                    (child.getTop() + childSize/2) : (child.getLeft() + childSize/2);

            int distanceFromCenter = Math.abs(center - displaySize/2);

            if(distanceFromCenter < bestDistanceFromCenter){
                if(view.getFirstVisiblePosition() == 0 && hasHeaderFooter){
                    //header is showing (it is child at 0, need to correct)
                    if(i==0){
                        //child is the header, skip it
                        continue;
                    } else {
                        //child is not the header but its visible so reported index should be reduced by 1
                        childAtCenter = getFirstVisiblePosition(view) + i - 1;
                    }
                } else {
                    //header not showing, allow normal processing
                    childAtCenter = getFirstVisiblePosition(view) + i;
                }
                bestDistanceFromCenter = distanceFromCenter;
            } else {
                break;
            }
        }

        setSelectedItemInternal(childAtCenter, true);
    }

    /** this method is used to smoothScrollToPositionFromTop since the native one is buggy */
    private void smoothScrollToPositionFromTop(final AbsListView view, final int position, final int topOffset) {
        //handler required to force action to work
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                view.smoothScrollToPositionFromTop(position, topOffset);
            }
        });
    }

    /** position the picker's view at the middle of the data-set */
    private void positionPickerInMiddle(PickerAdapter adapter){
        selectedItem = wrapAround && getData() != null && getData().size() > 0 ? adapter.MIDDLE : 0;
        previouslySelectedItem = selectedItem;
        currentBestCandidateForSelection = selectedItem;
        if(childSize > 0) setSelectedItemInternal(selectedItem, false);
    }

    /** return the relative distance between 0 and 1  of the specified list item from being
     * the selected item, where 0 is currently selected and 1 being least likely to be selected
     */
    private float getRelativeDistanceFromSelection(View view){
        float selectionStart = (displaySize - childSize)/2;
        float selectionEnd = selectionStart + childSize;
        float viewCenter = (getOrientation() == VERTICAL ?
                view.getTop() : view.getLeft()) + childSize/2;

        if(viewCenter > selectionStart && viewCenter < selectionEnd){
            //is selected
            return 0;
        } else if(viewCenter <= selectionStart){
            //before
            return Math.min(1, (selectionStart - viewCenter)/selectionStart);
        } else {
            //after
            return Math.min(1, (viewCenter - selectionEnd)/selectionStart);
        }
    }

    /** called when an item shift its position in the display, this method is used
     * to set a visual effect on the moving items relative to the selected item. This
     * method may be overridden to create custom effects
     *
     * @param view the view being manipulated
     * @param distanceFromSelection the relative distance of the view from the selected position between 0 and 1
     *                              where 0 being currently selected and 1 being afar from selection
     */
    protected void applyItemTransformation(View view, float distanceFromSelection){
        if(childSize < displaySize) {
            float scale =1-0.4f*distanceFromSelection;
            view.setScaleX(scale);
            view.setScaleY(scale);
            view.setAlpha(1 - distanceFromSelection);
        }
    }

    /** Create a view with arrow drawable based on the view orientation and button type to be used
     * when no other button resource is supplied
     */
    private View getDefaultButton(BUTTON button){
        int big = (int)(DEFAULT_BUTTONS_LARGE_DIMENSION * getResources().getDisplayMetrics().density);
        int small = (int)(DEFAULT_BUTTONS_SMALL_DIMENSION * getResources().getDisplayMetrics().density);
        int margin = (int)(DEFAULT_BUTTONS_MARGIN * getResources().getDisplayMetrics().density);
        Bitmap bitmap = getOrientation() == VERTICAL ?
                Bitmap.createBitmap(big,small, Bitmap.Config.ARGB_8888) :
                Bitmap.createBitmap(small,big, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(buttonsColor);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);

        Path path = new Path();
        if(button == BUTTON.PREV) {
            if (getOrientation() == VERTICAL) {
                path.moveTo(0, small);
                path.lineTo(big / 2, 0);
                path.lineTo(big, small);
            } else {
                path.moveTo(small, 0);
                path.lineTo(0, big / 2);
                path.lineTo(small, big);
            }
        } else{
            //nextButton
            path.moveTo(0, 0);
            if (getOrientation() == VERTICAL) {
                path.lineTo(big / 2, small);
                path.lineTo(big, 0);
            } else {
                path.lineTo(small, big / 2);
                path.lineTo(0, big);
            }
        }
        path.close();

        canvas.drawPath(path, paint);

        BitmapDrawable drawable = new BitmapDrawable(getResources(),bitmap);

        ImageView buttonView = new ImageView(getContext());
        buttonView.setImageDrawable(drawable);
        LayoutParams params =
                new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        if(button == BUTTON.PREV){
            if(getOrientation() == VERTICAL){
                params.bottomMargin = margin;
            } else {
                params.leftMargin = margin;
            }
        } else {
            //nextButton
            if(getOrientation() == VERTICAL){
                params.topMargin = margin;
            } else {
                params.rightMargin = margin;
            }
        }
        buttonView.setLayoutParams(params);

        return buttonView;
    }

    private int getFirstVisiblePosition(AbsListView listView) {
        return fixFirstVisiblePosition(listView.getFirstVisiblePosition());
    }

    private int fixFirstVisiblePosition(int position){
        return Math.max(0, hasHeaderFooter ? position - 1 : position);
    }

    /** if wrapping is enabled return the closets instance if the wrapped item, this allow for better
     * scroll handling when done in programmatic manner*/
    private int getClosestWrappedPosition(int currentlySelectedItem, int unwrappedPosition){
        if(getData() == null || getData().size() == 0) return 0;

        if(!wrapAround) return unwrappedPosition;

        //ensure unwrappedPosition is really unwrapped
        unwrappedPosition = unwrappedPosition % getData().size();
        int unwrappedCurrentItem = currentlySelectedItem % getData().size();

        //same underlying position
        if(unwrappedPosition == unwrappedCurrentItem) return currentlySelectedItem;

        int distanceFromBelow = unwrappedCurrentItem >= unwrappedPosition ?
                unwrappedCurrentItem - unwrappedPosition :
                getData().size() - unwrappedPosition + unwrappedCurrentItem;

        int distanceFromAbove = unwrappedCurrentItem <= unwrappedPosition ?
                unwrappedPosition - unwrappedCurrentItem :
                getData().size() - unwrappedCurrentItem + unwrappedPosition;

        if(distanceFromBelow <= distanceFromAbove && currentlySelectedItem - distanceFromBelow >=0){
            return currentlySelectedItem - distanceFromBelow;
        } else {
            return currentlySelectedItem + distanceFromAbove;
        }
    }

    /** convert a data-set position to a fixed index appropriate the the handling of this picker,
     * this method is only used to return position numbers to the callback interface and the
     * getSelectedItem call, it is used primarily when data-set is being scrambled for display
     * and needed to be unscrambled before informing the user
     *
     * @param position the original position as provided by the adapter
     * @return the true position of the item in the data-set
     */
    protected int fixPosition(int position){
        return position;
    }

    /** convert the specified wrapped position into an unwrapped version coresponding
     * real position in the data set
     */
    protected int getUnwrappedPosition(int position){
        return getData() != null && getData().size() > 0 ? position % getData().size() : 0;
    }
}