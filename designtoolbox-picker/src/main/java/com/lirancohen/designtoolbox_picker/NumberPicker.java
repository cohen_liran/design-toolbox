package com.lirancohen.designtoolbox_picker;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Liran on 1/31/2016.
 *
 * A specialized picker which display numbers in reverse order and also allow number formatting
 */
public class NumberPicker extends Picker {

    private String format;

    public NumberPicker(Context context) {
        super(context);
    }

    public NumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /** return the currently set format to use when displaying the numbers or null if no format is set*/
    public String getFormat() {
        return format;
    }

    /** set the format to use when displaying the numbers or null to not format numbers
     *
     * @param format a format string to use when displaying the data, format is
     *               applied by calling <i>String.format(String format, int number)</i>.
     *               common formats include %02d (two digit number with leading zero) and null
     */
    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    protected void getExtendingAttributes(Context context, AttributeSet attrs) {
        if(context == null ||attrs == null) return;

        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.NumberPicker);

        if(attributes == null) return;

        format = attributes.getString(R.styleable.NumberPicker_format);

        attributes.recycle();
    }

    @Override
    protected PickerAdapter getDefaultAdapter(List data) {
        List<Integer> dataInteger = new ArrayList<Integer>();
        for(Object o : data){
            dataInteger.add((Integer) o);
        }
        return new PickerAdapter<Integer>(getContext(), -1, dataInteger){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null){
                    convertView = new TextView(getContext());
                    ((TextView) convertView).setTextSize(getTextSize());
                    ((TextView) convertView).setTextColor(getTextColor());
                    AbsListView.LayoutParams params = new AbsListView.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    convertView.setLayoutParams(params);
                    ((TextView)convertView).setGravity(Gravity.CENTER);
                }

                if(getData().size() > 0) {
                    Integer item = getItem(fixPosition(getDataSetPosition(position)));
                    String itemString = format != null ? String.format(format, item) : item.toString();
                    ((TextView) convertView).setText(itemString);
                }

                return convertView;
            }
        };
    }

    @Override
    public int getSelectedItem() {
        return super.getSelectedItem();
    }



    @Override
    public void setSelectedItem(int selectedItem, boolean animate) {
        super.setSelectedItem(fixPosition(selectedItem), animate);
    }

    @Override
    protected int fixPosition(int position){
        if(getData() == null  || getData().size() == 0) return 0;
        return getData().size() -1 - position;
    }
}
