package com.lirancohen.designtoolbox_picker;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by Liran on 1/30/2016.
 *
 * A specialized adapter used with the Picker class to enable wrapAround feature.
 * Note that if extending this class's <i>getView</i> method you should always use <i>getItem(position)</i>
 * method to get the current item and avoid getting the item directly from the data-set yourself. this in order
 * to enable the wrapAround feature
 */
public class PickerAdapter<T> extends ArrayAdapter<T> {

    public static final int MAX_VALUE = Integer.MAX_VALUE-2; // -2 since picker also use footer and header

    /** the middle item handled by this adapter, this value is used in wrapAround mode
     * to help position the listView in its center allowing seemingly infinite scroll to both directions */
    public final int MIDDLE;

    private final List<T> data;

    private boolean wrapAround = false;
    private int poolSize = 0;

    public PickerAdapter(Context context, int resource, T[] objects) {
        super(context, resource, objects);
        this.poolSize = objects.length;
        MIDDLE = poolSize > 0 ? MAX_VALUE/2 - (MAX_VALUE/2) % poolSize : 0;
        data = Arrays.asList(objects);
    }

    public PickerAdapter(Context context, int resource, int textViewResourceId, T[] objects) {
        super(context, resource, textViewResourceId, objects);
        this.poolSize = objects.length;
        MIDDLE = poolSize > 0 ? MAX_VALUE/2 - (MAX_VALUE/2) % poolSize : 0;
        data = Arrays.asList(objects);
    }

    public PickerAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
        this.poolSize = objects.size();
        MIDDLE = poolSize > 0 ? MAX_VALUE/2 - (MAX_VALUE/2) % poolSize : 0;
        data = objects;
    }

    public PickerAdapter(Context context, int resource, int textViewResourceId, List<T> objects) {
        super(context, resource, textViewResourceId, objects);
        this.poolSize = objects.size();
        MIDDLE = poolSize > 0 ? MAX_VALUE/2 - (MAX_VALUE/2) % poolSize : 0;
        data = objects;
    }

    /** set the wrapAround feature of the adapter on and off, this should not be used outside of the native
     * Picker class implementation to allow proper data handling
     *
     * @param wrapAround Whether or not this adapter should loop its data-set
     */
    public void setWrapAround(boolean wrapAround){
        this.wrapAround = wrapAround;
    }

    /** return the number of items handled by this adapter, do not override this method to allow
     * propper wrapAround handling */
    @Override
    public int getCount() {
        return wrapAround ? MAX_VALUE : poolSize;
    }

    @Override
    public T getItem(int position) {
        return super.getItem(getDataSetPosition(position));
    }

    /** Return the data-set position co-responding to the supplied index so conversion between
     * wrapped mode and unwrapped mode are easy to handle
     * @param position the actual position passed by the adapter after wrapping if enabled
     * @return the position in the data-set of the wrapped item
     */
    public int getDataSetPosition(int position){
        return poolSize > 0 ? position % poolSize : 0;
    }

    @Override
    public void notifyDataSetChanged() {
        throw new UnsupportedOperationException("Data set cannot be updated after assignment, only replaced.");
    }

    /** return the underlying data set managed by this adapter as a list */
    public List<T> getData(){
        return data;
    }

    @Override
    public void add(T object) {
        throw new UnsupportedOperationException("Data set cannot be updated after assignment, only replaced.");
    }

    @Override
    public void addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException("Data set cannot be updated after assignment, only replaced.");
    }

    @Override
    public void addAll(T... items) {
        throw new UnsupportedOperationException("Data set cannot be updated after assignment, only replaced.");
    }

    @Override
    public void insert(T object, int index) {
        throw new UnsupportedOperationException("Data set cannot be updated after assignment, only replaced.");
    }

    @Override
    public void remove(T object) {
        throw new UnsupportedOperationException("Data set cannot be updated after assignment, only replaced.");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Data set cannot be updated after assignment, only replaced.");
    }
}
