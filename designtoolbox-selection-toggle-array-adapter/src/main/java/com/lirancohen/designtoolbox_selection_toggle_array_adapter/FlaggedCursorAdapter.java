package com.lirancohen.designtoolbox_selection_toggle_array_adapter;

import android.content.Context;
import android.database.Cursor;
import android.util.SparseArray;
import android.widget.CursorAdapter;

/**
 * Created by Liran on 1/10/2016.
 *
 * A specialized CursorAdapter capable of maintaining a boolean state flag of its assigned items,
 * an item of this adapter is stated as flag only of the flag supplied is true, this adapter can
 * only be used if the underlying cursor contain a unique id column
 */
public abstract class FlaggedCursorAdapter extends CursorAdapter {

    private SparseArray<Long> flaggedItems = new SparseArray<>();
    protected String idColumnName;
    protected int idColumnIndex;

    public FlaggedCursorAdapter(Context context, Cursor c, String idColumnName) {
        super(context, c);
        setIdColumn(c, idColumnName);
    }

    public FlaggedCursorAdapter(Context context, Cursor c, boolean autoRequery, String idColumnName) {
        super(context, c, autoRequery);
        setIdColumn(c, idColumnName);
    }

    public FlaggedCursorAdapter(Context context, Cursor c, int flags, String idColumnName) {
        super(context, c, flags);
        setIdColumn(c, idColumnName);
    }

    private void setIdColumn(Cursor c, String indexColumn){
        this.idColumnName = indexColumn;
        this.idColumnIndex = c.getColumnIndexOrThrow(indexColumn);
    }

    /** return an array containing all the items currently checked (as an key-value pair matching
     * the position of the item in the cursor and the unique id (as long) value */
    public SparseArray<Long> getFlaggedItems(){
        return flaggedItems;
    }

    /** return an array containing the positions of all the flagged items */
    public int[] getFlaggedPositions(){
        int flaggedItemsCount = flaggedItems.size();

        int[] flaggedItemsPositions = new int[flaggedItemsCount];

        for(int i=0; i<flaggedItemsCount; i++){
            flaggedItemsPositions[i] = flaggedItems.keyAt(i);
        }

        return flaggedItemsPositions;
    }

    /** return whether or not the item at the supplied index is flagged */
    public boolean isFlagged(int index){
        return flaggedItems.get(index) != null;
    }

    /** set the flag of the item at the specified index.
     * this will only take effect on screen after calling <i>notifyDataSetChanged</i>*/
    public void setFlagged(int index, boolean flag){
        if(flag){
            Cursor c = getCursor();
            c.moveToPosition(index);
            flaggedItems.put(index, c.getLong(idColumnIndex));
        } else {
            flaggedItems.remove(index);
        }
    }

    /** set the flag for all the items at the specified indexes.
     * this will only take effect on screen after calling <i>notifyDataSetChanged</i>*/
    public void setFlagged(int[] indexes, boolean flag){

        if(flag){
            Cursor c = getCursor();
            for(int i :indexes) {
                c.moveToPosition(i);
                flaggedItems.put(i, c.getLong(idColumnIndex));
            }
        } else {
            for(int i : indexes) {
                flaggedItems.remove(i);
            }
        }

        for(int i : indexes) setFlagged(i, flag);
    }

    /** set the flag to all the items of this adapter */
    public void flagAll(boolean flag){
        if(flag){
            Cursor c = getCursor();
            c.moveToFirst();
            while(!c.isAfterLast()){
                flaggedItems.put(c.getPosition(), c.getLong(idColumnIndex));
                c.moveToNext();
            }
        } else {
            flaggedItems.clear();
        }
    }
}
