package com.lirancohen.designtoolbox_selection_toggle_array_adapter;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.CheckBox;

import java.util.List;

/**
 * Created by Liran on 1/10/2016.
 *
 * A specialized adapter used to display a checkboxed list to enable item selection,
 * while using this adapter it is required to call <i>syncCheckbox</i> on getView after
 * list item view has been inflated, additional handling of checkboxes, and checked state
 * is not required. passed item layout resource must contain a CheckBox
 * view which can be handled by this adapter.
 *
 * Note: for proper animation handling avoid setting visibility state and alpha for the CheckBox view,
 * you should also note that the visibility state of the checkbox is changed from VISIBLE to GONE
 * during animation and layout inflation so margin usage should be taken into account to avoid
 * flickering transitions.
 *
 * Note: While this adapter disable the touchable,focusable and clickable state of the checkbox view
 * it does not enforce descendant focus blocking on the item view itself. unless item contain
 * a focus requiring views it is recommended that item layout will set its
 * <i>android:descendantFocusability</i> to "blockDescendant".
 */
public class SelectionToggleArrayAdapter<T> extends  FlaggedArrayAdapter<T> {

    private int checkboxId;
    private boolean selectionMode;
    private ViewGroup container;
    private int toggleAnimationDuration = 300;

    public SelectionToggleArrayAdapter(Context context, int resource, int checkboxId, boolean selectionMode) {
        super(context, resource);
        this.checkboxId = checkboxId;
        this.selectionMode = selectionMode;
    }

    public SelectionToggleArrayAdapter(Context context, int resource, int textViewResourceId, int checkboxId, boolean selectionMode) {
        super(context, resource, textViewResourceId);
        this.checkboxId = checkboxId;
        this.selectionMode = selectionMode;
    }

    public SelectionToggleArrayAdapter(Context context, int resource, int checkboxId, boolean selectionMode ,T[] objects) {
        super(context, resource, objects);
        this.checkboxId = checkboxId;
        this.selectionMode = selectionMode;
    }

    public SelectionToggleArrayAdapter(Context context, int resource, int textViewResourceId, int checkboxId, boolean selectionMode, T[] objects) {
        super(context, resource, textViewResourceId, objects);
        this.checkboxId = checkboxId;
        this.selectionMode = selectionMode;
    }

    public SelectionToggleArrayAdapter(Context context, int resource, int checkboxId, boolean selectionMode, List<T> objects) {
        super(context, resource, objects);
        this.checkboxId = checkboxId;
        this.selectionMode = selectionMode;
    }

    public SelectionToggleArrayAdapter(Context context, int resource, int textViewResourceId, int checkboxId, boolean selectionMode, List<T> objects) {
        super(context, resource, textViewResourceId, objects);
        this.checkboxId = checkboxId;
        this.selectionMode = selectionMode;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = super.getView(position, convertView, parent);
        syncCheckbox(position, convertView, parent);
        return convertView;
    }

    /** process the checkbox state, this should be called on getView and
     * to be supplied with the inflated list item layout
     *
     * @param position The position of the item being processed
     * @param convertView The basic list view item view, this must be called after view has been inflated
     * @param parent The ViewGroup managed by this adapter
     */
    public void syncCheckbox(int position, View convertView, ViewGroup parent){

        if(convertView == null) throw new NullPointerException("cannot sync checkbox state of null object 'convertView', should call syncCheckbox() after view has been inflated");

        container = parent;
        CheckBox checkBox = (CheckBox) convertView.findViewById(checkboxId);
        checkBox.setVisibility(selectionMode ? View.VISIBLE : View.GONE);
        checkBox.setFocusable(false);
        checkBox.setFocusableInTouchMode(false);
        checkBox.setClickable(false);

        checkBox.setChecked(isFlagged(position));
    }

    /** set if the associated list and this adapter should enter selection mode
     * displaying checkboxes next to items so they can be flagged.
     * if list items are already present the checkboxes will animate into the view
     */
    public void setSelectionMode(boolean selectionMode){
        if(this.selectionMode != selectionMode) {
            this.selectionMode = selectionMode;

            if (container != null) {
                int childCount = container.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    View checkBox = container.getChildAt(i).findViewById(checkboxId);
                    if (selectionMode) {
                        animateIn(checkBox);
                    } else {
                        animateOut(checkBox);
                    }
                }
            }
        }
    }

    /** return the time in milliseconds it take for the view to convert from normal
     * mode to selection mode when state change after layout already been inflated on screen
     */
    public int getToggleAnimationDuration() {
        return toggleAnimationDuration;
    }

    /** set the time in milliseconds it take for the view to convert from normal
     * mode to selection mode when state change after layout already been inflated on screen
     */
    public void setToggleAnimationDuration(int toggleAnimationDuration) {
        this.toggleAnimationDuration = toggleAnimationDuration;
    }

    /** return if this adapter's list currently set in selection mode */
    public boolean getSelectionMode(){
        return selectionMode;
    }

    private void animateIn(final View v){
        v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        final int fullWidth = v.getMeasuredWidth();

        ValueAnimator animator = ValueAnimator.ofFloat(v.getAlpha() < 1 ? v.getAlpha() : 0f, 1f);
        animator.setDuration(toggleAnimationDuration);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                v.getLayoutParams().width = 0;
                v.setAlpha(0f);
                v.setVisibility(View.VISIBLE);
                v.requestLayout();
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (Float) animation.getAnimatedValue();
                v.setAlpha(value);
                v.getLayoutParams().width = Math.round(fullWidth * value);
                v.requestLayout();
            }
        });
        animator.start();
    }

    private void animateOut(final View v){
        v.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        final int fullWidth = v.getMeasuredWidth();
        ValueAnimator animator = ValueAnimator.ofFloat(v.getAlpha() > 0 ? v.getAlpha() : 1f, 0f);
        animator.setDuration(toggleAnimationDuration);
        animator.setInterpolator(new DecelerateInterpolator());
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                v.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (Float) animation.getAnimatedValue();
                v.setAlpha(value);
                v.getLayoutParams().width = Math.round(fullWidth * value);
                v.requestLayout();
            }
        });
        animator.start();
    }

    @Override
    public void flagAll(boolean flag) {
        super.flagAll(flag);
        //update view
        if(container != null){
            int childCount = container.getChildCount();
            for(int i=0; i<childCount; i++){
                CheckBox checkBox = (CheckBox) container.getChildAt(i).findViewById(checkboxId);
                if(checkBox != null){
                    checkBox.setChecked(flag);
                }
            }
        }
    }
}
