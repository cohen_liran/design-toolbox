package com.lirancohen.designtoolbox_selection_toggle_array_adapter;

import android.content.Context;
import android.util.SparseArray;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by Liran on 1/10/2016.
 *
 * A specialized ArrayAdapter capable of maintaining a boolean state flag of its assigned items,
 * an item of this adapter is stated as flag only of the flag supplied is true
 */
public class FlaggedArrayAdapter<T> extends ArrayAdapter<T> {

    private SparseArray<T> flaggedItems = new SparseArray<>();

    public FlaggedArrayAdapter(Context context, int resource) {
        super(context, resource);
    }

    public FlaggedArrayAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public FlaggedArrayAdapter(Context context, int resource, T[] objects) {
        super(context, resource, objects);
    }

    public FlaggedArrayAdapter(Context context, int resource, int textViewResourceId, T[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public FlaggedArrayAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
    }

    public FlaggedArrayAdapter(Context context, int resource, int textViewResourceId, List<T> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    /** return an array containing all the items currently checked */
    public SparseArray<T> getFlaggedItems(){
        return flaggedItems;
    }

    /** return an array containing the positions of all the flagged items */
    public int[] getFlaggedPositions(){
        int flaggedItemsCount = flaggedItems.size();

        int[] flaggedItemsPositions = new int[flaggedItemsCount];

        for(int i=0; i<flaggedItemsCount; i++){
            flaggedItemsPositions[i] = flaggedItems.keyAt(i);
        }

        return flaggedItemsPositions;
    }

    /** return whether or not the item at the supplied index is flagged */
    public boolean isFlagged(int index){
        return flaggedItems.get(index) != null;
    }

    /** set the flag of the item at the specified index.
     * this will only take effect on screen after calling <i>notifyDataSetChanged</i>*/
    public void setFlagged(int index, boolean flag){
        if(flag){
            flaggedItems.put(index, getItem(index));
        } else {
            flaggedItems.remove(index);
        }
    }

    /** set the flag for all the items at the specified indexes.
     * this will only take effect on screen after calling <i>notifyDataSetChanged</i>*/
    public void setFlagged(int[] indexes, boolean flag){
        for(int i : indexes) setFlagged(i, flag);
    }

    /** set the flag to all the items of this adapter */
    public void flagAll(boolean flag){
        if(flag){
            for(int i=0; i<getCount(); i++){
                flaggedItems.put(i, getItem(i));
            }
        } else {
            flaggedItems.clear();
        }
    }
}
