package com.lirancohen.designtoolbox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lirancohen.designtoolbox_picker.Picker;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Picker picker = (Picker) findViewById(R.id.picker);

        List<Integer>  values = new ArrayList<>();
        int i=0;
        while(i < 140){
            values.add(i);
            i++;
        }
        picker.setDefaultAdapter(values);
    }
}
