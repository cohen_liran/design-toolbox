package com.lirancohen.designtoolbox_styleable_dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Liran on 1/14/2016.
 *
 * A dialog which support full appearance customization
 */
public class StyleableDialog extends Dialog implements DialogInterface{

    private static final String TAG = "StyleableDialog";

    /** a layout used as the body of the dialog */
    private int windowView = R.layout.dialog_base;

    /** a layout used as the content of the dialog */
    private View contentView;

    /** a drawable to act as the background of the dialog window */
    private Drawable windowBackground;

    /** an icon to be displayed next to the title */
    private Drawable icon;

    /** the title of the dialog */
    private String title;

    /** the title divider of the dialog */
    private Drawable titleDivider;

    /** the message which this dialog display */
    private String message;

    private int buttonLayout = R.layout.dialog_base_button;

    private boolean isCancelable;

    private OnShowListener onShowListener;

    /** a map of text-clickListener to be displayed at the buttonbar of the dialog */
    private List<Map<String, OnClickListener>> buttons = new ArrayList<>();

    private WeakReference<Activity> ownerActivity;

    public StyleableDialog(Context context) {
        super(context);
        setOwnerActivity(context);
    }

    public StyleableDialog(Context context, int themeResId) {
        super(context, themeResId);
        setOwnerActivity(context);
    }

    protected StyleableDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setOwnerActivity(context);
        setCancelable(cancelable);
        setOnCancelListener(cancelListener);
    }

    /** Set a reference for the containing activity of this dialog, this is used
     * to later correct the size of the dialog's content view in case of an overflow
     * @param context a context which may be the owner activity
     */
    private void setOwnerActivity(Context context){
        if(context instanceof Activity){
            ownerActivity = new WeakReference<Activity>((Activity) context);
        } else {
            Log.w(TAG,"supplied context is not an activity, it is recommended to supply dialog with an activity context so it can resize itself properly if required");
        }
    }

    /** create a dialog view with the supplied setting, created view is later passed to onPostApplySettings().
     * any additional customization must be inserted to this method using the returned value*/
    protected ViewGroup applySettings(){

        if(windowBackground != null) getWindow().setBackgroundDrawable(windowBackground);

        ViewGroup body = (ViewGroup) LayoutInflater.from(getContext()).inflate(windowView,null,false);

        TextView titleView = (TextView) body.findViewById(R.id.dialog_title);
        if(titleView == null) throw new IllegalStateException("Dialog layout must contain a title view with id R.id.dialog_title");
        if(title != null) titleView.setText(title);

        ImageView iconView = (ImageView) body.findViewById(R.id.dialog_icon);
        if(iconView == null) throw new IllegalStateException("Dialog layout must contain an icon view with id R.id.dialog_icon");
        if(icon != null){
            iconView.setImageDrawable(icon);
        } else {
            iconView.setVisibility(View.GONE);
        }

        View dividerView = body.findViewById(R.id.dialog_title_divider);
        if(dividerView == null) throw new IllegalStateException("Dialog layout must contain a title divider view with id R.id.dialog_title_divider");
        if(titleDivider != null){
            if(Build.VERSION.SDK_INT >= 16){
                dividerView.setBackground(titleDivider);
            } else {
                dividerView.setBackgroundDrawable(titleDivider);
            }
        }

        TextView messageView = (TextView) body.findViewById(R.id.dialog_message);
        if(messageView == null) throw new IllegalStateException("Dialog layout must contain a message view with id R.id.dialog_message");
        if(message != null) messageView.setText(message);

        ViewGroup contentViewView = (ViewGroup) body.findViewById(R.id.dialog_content_view);
        if(contentViewView == null) throw new IllegalStateException("Dialog layout must contain a content view with id R.id.dialog_content_view");
        if (contentView != null) contentViewView.addView(contentView);

        View buttonBarDivider = body.findViewById(R.id.dialog_button_bar_divider);
        if(buttonBarDivider == null) throw new IllegalStateException("Dialog layout must contain a divider view with id R.id.dialog_button_bar_divider");
        if(buttons.size() == 0) buttonBarDivider.setVisibility(View.GONE);

        ViewGroup buttonBar = (ViewGroup) body.findViewById(R.id.dialog_button_bar);
        if(buttonBar == null) throw new IllegalStateException("Dialog layout must contain a button-bar view with id R.id.dialog_button_bar");

        for(Map<String,OnClickListener> buttonSpec : buttons){
            View button = LayoutInflater.from(getContext()).inflate(buttonLayout, buttonBar, false);
            for(final Map.Entry<String, OnClickListener> entry : buttonSpec.entrySet()) {
                if (button instanceof TextView) {
                    ((TextView) button).setText(entry.getKey());
                } else if (button instanceof ViewGroup) {
                    TextView textView = (TextView) button.findViewById(R.id.dialog_button);
                    if (textView != null) textView.setText(entry.getKey());
                }

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewGroup buttonBar = (ViewGroup) StyleableDialog.this.findViewById(R.id.dialog_button_bar);
                        int childCount = buttonBar.getChildCount();
                        for(int i=0; i<childCount; i++){
                            if(buttonBar.getChildAt(i) == v){
                                entry.getValue().onClick(StyleableDialog.this, i);
                            }
                        }
                    }
                });
            }
            buttonBar.addView(button);
        }

        if(!isCancelable && buttons.size() == 0) Log.w(TAG, "None cancelable dialog created with no buttons.");

        return body;
    }

    /** set the supplied view as the content view of the dialog, this method is required
     * to enable farther customization of dialog by extending classes, default implementation
     * does nothing
     */
    protected View onPostApplySetting(View body){
        return body;
    }

    @Override
    public void setContentView(int layoutResID) {
        contentView = LayoutInflater.from(getContext()).inflate(layoutResID, null, false);
    }

    @Override
    public void setContentView(View view) {
        contentView = view;
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        this.setContentView(view);
    }

    @Override
    public void setTitle(CharSequence title) {
        this.title = title != null ? title.toString() : "";
    }

    @Override
    public void setTitle(int titleId) {
        this.title = getContext().getString(titleId);
    }

    /** return the string supplied as the title of the dialog */
    public String getTitle() {
        return title;
    }

    /** set background of the dialog window */
    public void setWindowBackground(Drawable windowBackground) {
        this.windowBackground = windowBackground;
    }

    /** set the divider line between the title bar and the message */
    public void setTitleDivider(Drawable titleDivider) {
        this.titleDivider = titleDivider;
    }

    /** set the icon displayed next to the title of the dialog */
    public void setIcon(int icon) {
        if(Build.VERSION.SDK_INT >= 21){
            this.icon = getContext().getResources().getDrawable(icon, null);
        } else {
            this.icon = getContext().getResources().getDrawable(icon);
        }
    }

    /** set the icon displayed next to the title of the dialog */
    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    /** return the message used in this dialog */
    public String getMessage() {
        return message;
    }

    /** set the message used for this dialog */
    public void setMessage(String message) {
        this.message = message;
    }

    /** set the message used for this dialog */
    public void setMessage(int message) {
        this.message = getContext().getString(message);
    }

    /** set the layout used as the body of the dialog, layout must contain
     * the specific ids to be functional: R.id.dialog_title, R.id.dialog_message, R.id.dialog_title_divider,
     * R.id.dialog_button_bar, R.id.dialog_icon, R.id.dialog_content_view, R.id.dialog_button_bar_divider
     */
    public void setWindowView(int windowView) {
        this.windowView = windowView;
    }

    /** add a button setup to the button bar, buttons are added to the view in the order which
     * they where added with this call
     *
     * @return the newly added button index in the list
     */
    public int addButton(int text, OnClickListener listener){
        Map<String, OnClickListener> buttonMap = new HashMap<>();
        buttonMap.put(getContext().getString(text), listener);
        buttons.add(buttonMap);
        return buttons.size()-1;
    }

    /** add a button setup to the button bar, buttons are added to the view in the order which
     * they where added with this call
     *
     * @return the newly added button index in the list
     */
    public int addButton(String text, OnClickListener listener){
        Map<String, OnClickListener> buttonMap = new HashMap<>();
        buttonMap.put(text, listener);
        buttons.add(buttonMap);
        return buttons.size()-1;
    }

    /** remove a button from the button bar listing at a specific index if exist */
    public void removeButton(int index){
        if(index > 0 || index >= buttons.size()) return;

        buttons.remove(index);
    }

    /** remove all button listing from the button bar */
    public void removeAllButtons(){
        buttons.clear();
    }

    /** set the layout used instead of the default for buttons displayed in the button bar,
     * for text to be displayed in the button. layout either must be of Button or TextView type
     * or contain a child of with id R.id.dialog_button */
    public void setButtonLayout(int buttonLayout) {
        this.buttonLayout = buttonLayout;
    }

    @Override
    public void show() {
        if(!isShowing()){
            View body = onPostApplySetting(applySettings());
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.setContentView(body);
            super.setOnShowListener(new OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    fixOverflow();
                    if (onShowListener != null) onShowListener.onShow(StyleableDialog.this);
                }
            });
        }
        super.show();
    }

    @Override
    public void setCancelable(boolean flag) {
        super.setCancelable(flag);
        isCancelable = flag;
    }

    @Override
    public void setOnShowListener(OnShowListener listener) {
        onShowListener = listener;
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    /** return the height of the status bar of the containing activity*/
    private int getStatusBarHeight() {
        int result = 0;
        int resourceId = getContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getContext().getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /** fix the height of the dialog in case that its content view too large to be displayed in full */
    private void fixOverflow(){
        if(ownerActivity != null && contentView != null){
            Activity activity = ownerActivity.get();

            if(activity != null){
                int availableHeight = (int)(0.92*(activity.getWindow().getDecorView().getHeight() - getStatusBarHeight()));

                if(getWindow().getDecorView().getHeight() >= availableHeight){
                    //not enough room for the content view provided, need to enforce a more clever wrap content
                    int contentViewHeight = contentView.getLayoutParams().height;

                    contentView.getLayoutParams().height = 0;
                    getWindow().getDecorView().measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

                    //get dialog size without the content view so we can tell how much room we can use
                    int minimalSize = getWindow().getDecorView().getMeasuredHeight();

                    if(minimalSize >= availableHeight){
                        //dialog base is too big to be fixed, restore original value
                        contentView.getLayoutParams().height = contentViewHeight;
                    } else {
                        //set the maximum size possible for our content view so it wont overflow
                        contentView.getLayoutParams().height = availableHeight - minimalSize;
                    }

                    contentView.requestLayout();
                }
            }
        }
    }
}
