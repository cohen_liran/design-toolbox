package com.lirancohen.designtoolbox_expandinglinearlayout;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Liran on 04-Dec-16.
 */

public class ExpandingLinearLayout extends LinearLayout {

    public static final int DEFAULT_STATE_CHANGE_DURATION = 240;

    private static final int DEFAULT_HEADER_RESOURCE = R.layout.expanding_header_view;

    public enum State{
        COLLAPSED,
        EXPANDING,
        EXPANDED,
        COLLAPSING
    }

    private State currentState = State.COLLAPSED;
    private View headerView;
    private LinearLayout contentHolder;
    private OnClickListener headerClickListener;

    private ValueAnimator animator;
    private int stateChangeDuration = DEFAULT_STATE_CHANGE_DURATION;

    private OnStateChangeListener stateListener;
    private OnstateProgressUpdateListener progressListener;

    private int measuredWidth = 0;
    private String title;
    private int contentHolderTopPadding;
    private int contentHolderBottomPadding;
    private int contentHolderPaddinglessMaxHeight;
    private boolean isTitleSet = false;
    private Margins contentHolderMargins = new Margins();

    public ExpandingLinearLayout(Context context) {
        super(context);
        init(context, null);
    }

    public ExpandingLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ExpandingLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ExpandingLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs){
        setOrientation(VERTICAL);

        int headerResource = DEFAULT_HEADER_RESOURCE;

        if(attrs != null){
            TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ExpandingLinearLayout);

            headerResource = attributes.getResourceId(R.styleable.ExpandingLinearLayout_headerResource, DEFAULT_HEADER_RESOURCE);
            setAnimationDuration(attributes.getInt(R.styleable.ExpandingLinearLayout_stateChangeDuration, DEFAULT_STATE_CHANGE_DURATION));
            isTitleSet = hasAttribute(attributes, R.styleable.ExpandingLinearLayout_itemTitle, true);
            title = attributes.getString(R.styleable.ExpandingLinearLayout_itemTitle);
            currentState = attributes.getInt(R.styleable.ExpandingLinearLayout_initialState, 0) == 1 ?
                State.EXPANDED : State.COLLAPSED;

            attributes.recycle();
        }

        //add title view
        setHeaderView(headerResource);

        //add content holder
        contentHolder = new LinearLayout(context);
        contentHolder.setOrientation(VERTICAL);
        super.addView(contentHolder);
    }

    private boolean hasAttribute(TypedArray attributes, int index, boolean allowEmptyValue){
        if(Build.VERSION.SDK_INT >= 22 && allowEmptyValue){
            if (attributes.hasValueOrEmpty(index)){
                return true;
            }
        } else {
            if (attributes.hasValue(index)){
                return true;
            }
        }
        return false;
    }

    private boolean isReadyForContent(){
        return contentHolder != null && super.getChildCount() == 2;
    }

    @Override
    public void addView(View child) {
        if(isReadyForContent()) contentHolder.addView(child);
        else super.addView(child);
    }

    @Override
    public void addView(View child, int index) {
        if(isReadyForContent()) contentHolder.addView(child, index);
        else super.addView(child, index);
    }

    @Override
    public void addView(View child, int width, int height) {
        if(isReadyForContent()) contentHolder.addView(child, width, height);
        else super.addView(child, width, height);
    }

    @Override
    public void addView(View child, ViewGroup.LayoutParams params) {
        if(isReadyForContent()) contentHolder.addView(child, params);
        else super.addView(child, params);
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        if(isReadyForContent()) contentHolder.addView(child,index,params);
        else super.addView(child,index,params);
    }

    @Override
    public void removeView(View view) {
        if(isReadyForContent()) contentHolder.removeView(view);
        else super.removeView(view);
    }

    @Override
    public void removeViewAt(int index) {
        if(isReadyForContent()) contentHolder.removeViewAt(index);
        else super.removeViewAt(index);
    }

    @Override
    public void removeAllViews() {
        if(isReadyForContent()) contentHolder.removeAllViews();
        else super.removeAllViews();
    }

    /** return the the child view inside the collapsing area at the specified index */
    public View getCollapsingChildAt(int index) {
        return contentHolder != null ? contentHolder.getChildAt(index) : null;
    }

    /** return the the amount of child views inside the collapsing area */
    public int getCollapsingChildCount() {
        return contentHolder != null ? contentHolder.getChildCount() : 0;
    }

    @Override
    public int indexOfChild(View child) {
        if(isReadyForContent()) return contentHolder.indexOfChild(child);
        else return super.indexOfChild(child);
    }

    private OnClickListener internalHeaderClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(headerClickListener != null) headerClickListener.onClick(v);
            toggle();
        }
    };

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        measuredWidth = MeasureSpec.getSize(widthMeasureSpec);
        int height = 0;

        switch (currentState){
            case COLLAPSED:
                height = getHeaderHeight();
                if(contentHolder != null){
                    contentHolderBottomPadding = contentHolder.getPaddingBottom();
                    contentHolderTopPadding = contentHolder.getPaddingTop();
                    contentHolderPaddinglessMaxHeight = getContentSize() - contentHolder.getPaddingBottom() - contentHolder.getPaddingTop();
                    Margins margins = getMargins(contentHolder);
                    if(margins.getBottomMargin() != 0 || margins.getTopMargin() != 0){
                        contentHolderMargins = margins;
                        ((MarginLayoutParams)contentHolder.getLayoutParams()).setMargins(margins.getLeftMargin(), 0, margins.getRightMargin(), 0);
                    }
                }
                break;
            case EXPANDED:
                height = getContentSize() + getHeaderHeight();
                if(contentHolder != null){
                    contentHolderBottomPadding = contentHolder.getPaddingBottom();
                    contentHolderTopPadding = contentHolder.getPaddingTop();
                    contentHolderPaddinglessMaxHeight = getContentSize() - contentHolder.getPaddingBottom() - contentHolder.getPaddingTop();
                    contentHolderMargins = getMargins(contentHolder);
                }
                break;
            case EXPANDING:
            case COLLAPSING:
                height = getContentSize() + getHeaderHeight();
                Margins margins = getMargins(contentHolder);
                contentHolder.measure(
                        MeasureSpec.makeMeasureSpec(measuredWidth -getPaddingLeft() - getPaddingRight() - margins.getLeftMargin() - margins.getRightMargin(),MeasureSpec.EXACTLY),
                        MeasureSpec.makeMeasureSpec(contentHolder.getLayoutParams().height + contentHolder.getPaddingTop() + contentHolder.getPaddingBottom(),MeasureSpec.EXACTLY));
                break;
        }

        if(contentHolder != null) height += getMarginsHeight(contentHolder);

        setMeasuredDimension(measuredWidth, height + getPaddingTop() + getPaddingBottom());
    }

    protected int getHeaderHeight(){
        if(headerView != null){
            Margins margins = getMargins(headerView);
            headerView.measure(MeasureSpec.makeMeasureSpec(measuredWidth - getPaddingLeft() - getPaddingRight() - margins.getLeftMargin() -margins.getRightMargin(), MeasureSpec.EXACTLY), MeasureSpec.UNSPECIFIED);
            return headerView.getMeasuredHeight() + getMarginsHeight(headerView);
        } else {
            return 0;
        }
    }

    protected int getContentSize(){
        if(contentHolder != null){
            Margins containerMargins = getMargins(contentHolder);
            int contentHeight = 0;
            if(contentHolder.getLayoutParams().height == LayoutParams.WRAP_CONTENT) {
                for (int i = 0; i < contentHolder.getChildCount(); i++) {
                    View child = contentHolder.getChildAt(i);
                    if(child.getVisibility() != GONE) {
                        int widthSpec;
                        switch (child.getLayoutParams().width) {
                            case LayoutParams.MATCH_PARENT:
                                widthSpec = MeasureSpec.makeMeasureSpec(measuredWidth - getPaddingLeft() - getPaddingRight() - contentHolder.getPaddingRight() - contentHolder.getPaddingLeft() - containerMargins.getLeftMargin() - containerMargins.getRightMargin(), MeasureSpec.AT_MOST);
                                break;
                            case ViewGroup.LayoutParams.WRAP_CONTENT:
                                widthSpec = MeasureSpec.UNSPECIFIED;
                                break;
                            default:
                                widthSpec = MeasureSpec.makeMeasureSpec(contentHolder.getLayoutParams().width - getPaddingLeft() - getPaddingRight() - contentHolder.getPaddingRight() - contentHolder.getPaddingLeft() - containerMargins.getLeftMargin() - containerMargins.getRightMargin(), MeasureSpec.EXACTLY);
                                break;
                        }
                        child.measure(widthSpec, MeasureSpec.UNSPECIFIED);

                        contentHeight += child.getMeasuredHeight();
                        contentHeight += getMarginsHeight(child);
                    }
                }
            } else {
                return contentHolder.getLayoutParams().height + contentHolder.getPaddingTop() + contentHolder.getPaddingBottom();
            }
            contentHeight += (contentHolder.getPaddingTop() + contentHolder.getPaddingBottom());
            contentHolder.measure(
                    MeasureSpec.makeMeasureSpec(measuredWidth - getPaddingLeft() - getPaddingRight() - containerMargins.getLeftMargin() - containerMargins.getRightMargin(), MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(contentHeight, MeasureSpec.EXACTLY));

            return contentHeight;
        }
        return 0;
    }

    private int getMarginsHeight(View view){
        Margins margins = getMargins(view);
        return margins.getTopMargin() + margins.getBottomMargin();
    }

    private Margins getMargins(View view){
        Margins margins = new Margins();
        try {
            MarginLayoutParams marginParams = (MarginLayoutParams) view.getLayoutParams();
            margins.setLeftMargin(marginParams.leftMargin);
            margins.setTopMargin(marginParams.topMargin);
            margins.setRightMargin(marginParams.rightMargin);
            margins.setBottomMargin(marginParams.bottomMargin);
        } catch (Exception e){}
        return margins;
    }

    private void changeStateInstant(State newState){
        if(currentState == newState) return;
        if(animator != null){
            animator.cancel();
            animator = null;
        }
        contentHolder.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
        contentHolder.setPadding(contentHolder.getPaddingLeft(), contentHolderTopPadding, contentHolder.getPaddingRight(), contentHolderBottomPadding);
        currentState = newState;
        requestLayout();
        setHeaderState();
        notifyStateChange();
    }

    private void animate(final State newState){

        if(animator != null){
            animator.cancel();
            animator = null;
        }

        int startHeight = getHeight() - getHeaderHeight() - getPaddingTop() - getPaddingBottom();
        if(currentState == State.COLLAPSED && startHeight != 0 && getHeight() == getHeaderHeight()){
            //first time animating, should adjust views first
            startHeight = 0;
        }

        int finishHeight = newState == State.COLLAPSED ? 0 : getContentSize() + contentHolderMargins.getTopMargin() + contentHolderMargins.getBottomMargin();

        currentState = newState == State.COLLAPSED ? State.COLLAPSING : State.EXPANDING;
        setHeaderState();
        notifyStateChange();

        animator = ValueAnimator.ofInt(startHeight, finishHeight);
        float transitionFraction = Math.abs(startHeight - finishHeight) / ((float)(contentHolderPaddinglessMaxHeight + contentHolder.getPaddingTop() + contentHolder.getPaddingBottom() + contentHolderMargins.getTopMargin() + contentHolderMargins.getBottomMargin()));
        animator.setDuration((long) (stateChangeDuration * transitionFraction));

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                if(currentState != newState) {
                    int totalAccumulatedHeight = 0;
                    int newFullHeight = (int) animation.getAnimatedValue();

                    int newTopMargin = Math.min(contentHolderMargins.getTopMargin(), Math.max(0, newFullHeight - totalAccumulatedHeight));
                    totalAccumulatedHeight += newTopMargin;

                    int newTopPadding = Math.min(contentHolderTopPadding,  Math.max(0, newFullHeight - totalAccumulatedHeight));
                    totalAccumulatedHeight += newTopPadding;

                    int newHeight =  Math.min(contentHolderPaddinglessMaxHeight, Math.max(0, newFullHeight - totalAccumulatedHeight));
                    totalAccumulatedHeight += newHeight;

                    int newBottomPadding = Math.min(contentHolderBottomPadding, Math.max(0, newFullHeight - totalAccumulatedHeight));
                    totalAccumulatedHeight += newBottomPadding;

                    int newBottomMargin = Math.min(contentHolderMargins.getBottomMargin(), Math.max(0, newFullHeight - totalAccumulatedHeight));

                    contentHolder.setPadding(contentHolder.getPaddingLeft(), newTopPadding, contentHolder.getPaddingRight(), newBottomPadding);
                    ((MarginLayoutParams)contentHolder.getLayoutParams()).setMargins(
                            contentHolderMargins.getLeftMargin(),
                            newTopMargin,
                            contentHolderMargins.getRightMargin(),
                            newBottomMargin);

                    contentHolder.getLayoutParams().height = newHeight;
                    requestLayout();
                    if(progressListener != null){
                        progressListener.onStateProgressUpdate(ExpandingLinearLayout.this,
                                (newState == State.COLLAPSED || newState == State.COLLAPSING) ? State.EXPANDED : State.COLLAPSED,
                                newState, animation.getAnimatedFraction());
                    }
                }
            }
        });
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if(progressListener != null){
                    progressListener.onStateProgressUpdate(ExpandingLinearLayout.this,
                            (newState == State.COLLAPSED || newState == State.COLLAPSING) ? State.EXPANDED : State.COLLAPSED,
                            newState, 0);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                contentHolder.setPadding(contentHolder.getPaddingLeft(), contentHolderTopPadding, contentHolder.getPaddingRight(), contentHolderBottomPadding);
                int topMargin = 0;
                if(newState == State.EXPANDED) topMargin = contentHolderMargins.getTopMargin();

                int bottomMargin = 0;
                if(newState == State.EXPANDED) bottomMargin = contentHolderMargins.getBottomMargin();

                ((MarginLayoutParams)contentHolder.getLayoutParams()).setMargins(
                        contentHolderMargins.getLeftMargin(),
                        topMargin,
                        contentHolderMargins.getRightMargin(),
                        bottomMargin);

                contentHolder.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                currentState = newState;
                requestLayout();
                notifyStateChange();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                currentState = newState;
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                throw new UnsupportedOperationException();
            }
        });
        animator.start();
    }

    private void notifyStateChange(){
        if(progressListener != null){
            progressListener.onStateProgressUpdate(ExpandingLinearLayout.this, currentState == State.COLLAPSED ? State.EXPANDED : State.COLLAPSED, currentState, 1f);
        }
        if(stateListener != null){
            stateListener.onStateChange(this, currentState);
            if(currentState == State.COLLAPSED) stateListener.onCollapse(this);
            else if(currentState == State.EXPANDED) stateListener.onExpand(this);
        }
    }

    private void setHeaderState(){
        headerView.setActivated(currentState == State.EXPANDED || currentState == State.EXPANDING);
    }

    /** attach a listener to be notified when the collapse/expand state of the layout
     * is changing
     *
     * @param stateListener the listener to attach
     */
    public void setOnStateChangeListener(OnStateChangeListener stateListener) {
        this.stateListener = stateListener;
    }

    /** attach a listener to be notified as the collapse/expand process is progressing */
    public void setOnStateProgressUpdateListener(OnstateProgressUpdateListener progressListener){
        this.progressListener = progressListener;
    }

    /** return the currently set header view of the layout or null if not set */
    public View getHeaderView(){
        return headerView;
    }

    /** set the view to use as a header for this layout
     *
     * @return the inflated view used
     */
    public View setHeaderView(int headerResource){
        if(headerView != null){
            super.removeViewAt(0);
            headerView = null;
        }

        return setHeaderView(LayoutInflater.from(getContext()).inflate(headerResource, this, false));
    }

    /** set the view to use as a header for this layout
     *
     * @return the view used
     */
    public View setHeaderView(View view){
        if(this.headerView != null){
            super.removeViewAt(0);
            headerView = null;
        }

        headerView = view;
        headerView.setOnClickListener(internalHeaderClickListener);
        super.addView(headerView,0);
        if(isTitleSet) setTitle(title);
        setHeaderState();
        return headerView;
    }

    /** set the title of this view, by default title will only apply
     * to the first TextView descendant in the header view, title will
     * be retained when swapping header views
     */
    public void setTitle(String title){
        isTitleSet = true;
        if(headerView instanceof TextView){
            ((TextView) headerView).setText(title);
        } else if(headerView instanceof ViewGroup){
            for(int i=0; i<((ViewGroup) headerView).getChildCount(); i++){
                View child = ((ViewGroup) headerView).getChildAt(i);
                if(child instanceof TextView){
                    ((TextView) child).setText(title);
                    return;
                }
            }
        }
    }

    /** set the title of this view, by default title will only apply
     * to the first TextView descendant in the header view, title will
     * be retained when swapping header views
     */
    public void setTitle(int title){
        setTitle(getResources().getString(title));
    }

    /** return the current collapse/expand state of this view */
    public State getState(){
        return currentState;
    }

    /** set a click listener to be notified when the header item is clicked */
    public void setOnHeaderClickListener(OnClickListener clickListener){
        headerClickListener = clickListener;
    }

    /** expand this view if its not already expanded or expanding */
    public void expand(){
        if(currentState == State.EXPANDED || currentState == State.EXPANDING) return;
        animate(State.EXPANDED);
    }

    /** collapse this view if its not already collapsed or collapsing */
    public void collapse(){
        if(currentState == State.COLLAPSED || currentState == State.COLLAPSING) return;
        animate(State.COLLAPSED);
    }

    /** instantly expanding this view if its not already expanded, skipping animation if running */
    public void expandInstant(){
        changeStateInstant(State.EXPANDED);
    }

    /** instantly collapsing this view if its not already collapsed, skipping animation if running */
    public void collapseInstant(){
        changeStateInstant(State.COLLAPSED);
    }

    /** toggle the state of this view between expanded and collapsed */
    public void toggle(){
        if(currentState == State.EXPANDED || currentState == State.EXPANDING){
            collapse();
        } else if(currentState == State.COLLAPSED || currentState == State.COLLAPSING){
            expand();
        }
    }

    /** toggle the state of this view between expanded and collapsed instantly without animation*/
    public void toggleInstant(){
        if(currentState == State.EXPANDED || currentState == State.EXPANDING){
            collapseInstant();
        } else if(currentState == State.COLLAPSED || currentState == State.COLLAPSING){
            expandInstant();
        }
    }

    /** set the duration of the collapse/expand animation */
    public void setAnimationDuration(int duration) {
        this.stateChangeDuration = duration;
    }

    public interface OnStateChangeListener{
        void onStateChange(ExpandingLinearLayout layout, State state);
        void onCollapse(ExpandingLinearLayout layout);
        void onExpand(ExpandingLinearLayout layout);
    }

    public interface OnstateProgressUpdateListener{
        void onStateProgressUpdate(ExpandingLinearLayout layout, State oldState, State newState, float progress);
    }
}
