package com.lirancohen.designtoolbox_expandinglinearlayout;

/**
 * Created by Liran on 20-Jan-17.
 */

public class Margins {
    private int leftMargin, topMargin, bottomMargin, rightMargin;

    public Margins() {
    }

    public Margins(int leftMargin, int topMargin, int bottomMargin, int rightMargin) {
        this.leftMargin = leftMargin;
        this.topMargin = topMargin;
        this.bottomMargin = bottomMargin;
        this.rightMargin = rightMargin;
    }

    public int getLeftMargin() {
        return leftMargin;
    }

    public void setLeftMargin(int leftMargin) {
        this.leftMargin = leftMargin;
    }

    public int getTopMargin() {
        return topMargin;
    }

    public void setTopMargin(int topMargin) {
        this.topMargin = topMargin;
    }

    public int getBottomMargin() {
        return bottomMargin;
    }

    public void setBottomMargin(int bottomMargin) {
        this.bottomMargin = bottomMargin;
    }

    public int getRightMargin() {
        return rightMargin;
    }

    public void setRightMargin(int rightMargin) {
        this.rightMargin = rightMargin;
    }
}
