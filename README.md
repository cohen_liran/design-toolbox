# Android DesignToolbox#

* [Current version](https://bitbucket.org/cohen_liran/design-toolbox/overview#markdown-header-whats-in-the-current-version)

If you are like me than you probably asked yourself why are some of the most trivial design patterns in android UI development are so hard to reproduce, starting from lack of appropriate widgets and up to seemingly random style resource overriding just to get that nice look, ending up with a messy hard to maintain and even harder to read code. 

## YES! So what is this repository for? ###

This repository contain a bundle of helpful widgets and UI helper classes (such as adapters), aimed at providing enough tools for the everyday developer for an easier faster deployment of common UI patterns in Android development including:

* [Fully customize-able Picker widget](https://bitbucket.org/cohen_liran/design-toolbox/overview#markdown-header-picker)
* [Fully customize-able NumberPicker widget](https://bitbucket.org/cohen_liran/design-toolbox/overview#markdown-header-picker)
* [Fully customize-able Dialog component](https://bitbucket.org/cohen_liran/design-toolbox/overview#markdown-header-styleabledialog)
* [Array adapter which toggle its list between selection and normal display state](https://bitbucket.org/cohen_liran/design-toolbox/overview#markdown-header-selectiontogglearrayadapter)
* [Array adapter which manage a boolean flag applied to its data-set without changing it](https://bitbucket.org/cohen_liran/design-toolbox/overview#markdown-header-selectiontogglearrayadapter)
* [A ViewPager adapter which provide two-way access to its underlying fragments](https://bitbucket.org/cohen_liran/design-toolbox/overview#markdown-header-referencedpageradapter)
* [A LinearLayout which provide expand-collapse content with header](https://bitbucket.org/cohen_liran/design-toolbox/overview#markdown-header-expandablelinearlayout)

## How do I get set up? ###

### Setup using Gradle (jCenter)

* In android studio open the module:app build.gradle file
* Add the dependency of the desired module to the dependencies section of the file or add the -all dependency for the full bundle:
```
#!java

dependencies {
    ...
    compile 'com.lirancohen.designtoolbox:designtoolbox-all:0.0.24' //this example may not be the most recent release, see updates below
}
```

### Setup using Gradle (maven)

* In android studio open the projects build.gradle file
* add my maven repository to the repositories list like so:

```
#!Java

allprojects {
    repositories {
        ...
        maven {
            url 'https://dl.bintray.com/lirancr/maven/'
        }
    }
}
```

* In android studio open the module:app build.gradle file
* Add the dependency of the desired module to the dependencies section of the file or add the -all dependency for the full bundle:
```
#!java

dependencies {
    ...
    compile 'com.lirancohen.designtoolbox:designtoolbox-all:0.0.24' //this example may not be the most recent release, see updates below
}
```

### Setup using git

* Clone the project to your local machine and import the project in android studio as a module, process may vary between platforms

### Troubleshooting (Android studio installation using gradle)

**I'm getting an error 'Gradle cannot resolve resource....' after syncing**

Right now The DesignToolbox is only available from either jcenter or maven, if android studio is not set to search for the repository in one of those locations this error may be shown.
* Go to your project's build.gradle file and ensure you have either jcenter or maven set up as a repository source:

```
#!java

allprojects {
    repositories {
        ...
        //define jcenter as repo source
        jcenter()
        //OR define maven as repo source
        maven {
            url 'https://dl.bintray.com/lirancr/maven/'
        }
    }
}
```

## What's in the current version? ##

I'm glade you asked, here is a list of widgets and features available with the toolbox, each module can be either imported as a standalone(see list below) or in the DesignToolbox bundle

**Current version(bundle):**

```
#!java

compile 'com.lirancohen.designtoolbox:designtoolbox-all:0.0.24'
```

## Contribution guidelines ##

* This project is under [apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0), feel free to use it for you projects and expand it so we can all have a better toolset for future development.
* If you wish to contribute to this project submit your code/repo to me and ill add it to the project

## Who do I talk to? ##

* Me (Liran Cohen)

------------------------------

# Picker #

**Current standalone version:**

```
#!java

compile 'com.lirancohen.designtoolbox:designtoolbox-picker:0.1.7'
```

### About ###

![Screenshot_2016-02-26-17-28-52.jpg](https://bitbucket.org/repo/Rqkqak/images/2060744672-Screenshot_2016-02-26-17-28-52.jpg)

A scrolling picker widget which can be customized to display any type of content and also apply any appearance. This widget comes with an additional extension of the **Picker** called **NumberPicker**, which is a replacement for the native NumberPicker offered in android.

### Usage ###

* Simply add the picker widget as part of your layout like so:

```
#!xml

<com.lirancohen.designtoolbox_picker.Picker
       android:id="@+id/picker"
       android:layout_width="wrap_content"
       android:layout_height="wrap_content"/> 
```

* Assign simple data-set to be displayed by the picker (the following example show dataset as a list of strings but it may be any type of list object you like


```
#!java

List<String> names = new ArrayList<String>();
        names.add("Billy");
        names.add("Jane");
        names.add("Toby");
        
        Picker mPicker = (Picker) findViewById(R.id.picker);
        mPicker.setDefaultAdapter(names);
```
* Alternatively display a complex data representation by providing a custom adapter


```
#!java

mPicker.setAdapter(new PickerAdapter(context, resourceId, names){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                // custom adapter implementation
            }
        }));
```
* Retrieve current selection index in the underlying data-set

```
#!java

int currentSelectionIndex = mPicker.getSelectedItem();
        String selectedName = names.get(currentSelectionIndex);
```
* Optionally listen to selection changes by attaching a listener

```
#!java

mPicker.setOnSelectionChangeListener(new Picker.OnSelectionChangeListener() {
            @Override
            public void onSelectionChanged(int newSelection, int oldSelection, boolean isFinal) {
                //handle selection changes
            }
        });
```

### Custom XML attributes ###

* scrollable - [boolean] set whether or not the picker can be scrolled by dragging its scrolling part
* wrapAround - [boolean] set whether or not assigned data-set items should be looped when reaching the end
* showButtons - [boolean] set whether or not to display the next & previous buttons
* prevButton - [resource] set a drawable to act as the previous item button
* nextButton - [resource] set a drawable to act as the next item button
* itemDivider - [resource] set a drawable to act as a divider between items
* buttonsColor - [color] set the color of the buttons, only apply if using default buttons
* displaySize - [dimension] set the size (preferable in dp) of the display area of the picker, this does not include space taken by the buttons if shown
* textColor - [color] set the color of the text displayed by the items of the picker, only apply if using default adapter
* textSize - [color] set the size of the text displayed by the items of the picker, only apply if using default adapter
* format (NumberPicker only) - [String] the format to use when displaying the numbers in the picker most common one is %02d which will display a leading zero before numbers if not bi-decimal

------------------------------

# SelectionToggleArrayAdapter#

**Current standalone version:**

```
#!java

compile 'com.lirancohen.designtoolbox:designtoolbox-selection-toggle-array-adapter:0.0.4'
```

### About ###

![Untitled-1.gif](https://bitbucket.org/repo/Rqkqak/images/1725021118-Untitled-1.gif)

A specialized adapter which is used to create the commonly seen checkboxed list UI pattern. This widget comes with a simplified version of the adapter called **FlaggedArrayAdapter** which expose setting a boolean flag state to an item of the list without the checkbox or any ui representation (it may be useful for cases where state need to be tracked for later processing or simply to provide the basis for another type of adapter which utilize this pattern), this package also include a version of the adapter which can be used with Cursors

### Usage ###

* Create a layout with a plain ListView widget that the adapter will be applied to
* Create a custom list item layout, the layout should include at the very least a checkbox view to be displayed when traversing between normal and selection mode:

```
#!XML

<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
              android:orientation="horizontal"
              android:layout_width="match_parent"
              android:layout_height="match_parent"
              android:gravity="center_vertical">

    <CheckBox
        android:id="@+id/checkbox"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"/>

    <TextView
        android:layout_marginLeft="20dp"
        android:id="@+id/listitemtext"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:textSize="26sp"/>

</LinearLayout>
```

* Create a new SelectionToggleArrayAdapter instance using one of the available constructors (for simplicity of this tutorial i used one of the default adapter implementations but this can just as well be custom extending class with overriding the getView function):


```
#!JAVA

String[] names = new String[]{"Jeff","Toby","Mark"};

        SelectionToggleArrayAdapter adapter = new SelectionToggleArrayAdapter<String>(context, resourceId, R.id.listitemtext, R.id.checkbox, false, names);
```

* Apply the adapter to the list view


```
#!java

mListView.setAdapter(adapter);
```

* Start the selection, in my example i wanted to enter/exit selection mode on long item click):

```
#!java

mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                //reset all the items back to unchecked before changing mode
                adapter.flagAll(false);
                //transfer listView to selection mode (checkbox mode)
                adapter.setSelectionMode(!adapter.getSelectionMode());
                return true;
            }
        });
```

* Set an item of the list as checked or unchecked, in my example i wanted to change the checked state of the clicked item:

```
#!java

mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(adapter.getSelectionMode()){
                    adapter.setFlagged(position, !adapter.isFlagged(position));
                    adapter.notifyDataSetChanged();
                }
            }
        });
```

Remember that the adapter save the flag state separate from its underlying data-set so maintaining the state on configuration changes is up to you.

------------------------------

# ReferencedPagerAdapter#

**Current standalone version:**

```
#!java

compile 'com.lirancohen.designtoolbox:designtoolbox-referenced-pager-adapter:0.0.2'
```

### About ###

Every now and then you come across the need to communicate between fragments in your ViewPager layout and its containing activity. Its probably at this point that you discover that while using interfaces to talk to the activity is well defined, BUT sending commands to specific fragments in the page from the activity is basically a huge hassle. 

This pager adapter automatically (and safely!) hold a reference to any of its underlying children allowing you to communicate with each of the fragments from the activity without holding to a hard reference in your activity's code.

### Usage ###

* Create a layout containing a ViewPager (available from the support library v4 by google)
* Create an ReferencedPagerAdapter:

```
#!java

ReferencedPagerAdapter adapter = new ReferencedPagerAdapter(getSupportFragmentManager()){
            @Override
            public Fragment getItem(int position) {
                Fragment page;
                switch(position){
                    page = new MyPageFragment();
                    break;
                    ...
                }
                return page;
            }
        };
```

* Apply adapter to your view pager:

```
#!java

mViewPager.setAdapter(adapter);
```
* Get a the the fragment at a specific position in the pager:


```
#!java

Fragment fragmentAtPosition = adapter.getFragmentAt(position);
```
* from here you can freely send a command to your fragment. for safety always make sure the fragment was created first before sending commands and ensure it is of the correct type:

```
#!java

if(fragment instanceof MyPageFragment){ //instanceof also return false for null cases
            ((MyPageFragment) fragment).doPageRelatedStuff();
        }
```


------------------------------

# StyleableDialog #

**Current standalone version:**

```
#!java

compile 'com.lirancohen.designtoolbox:designtoolbox-styleable-dialog:0.0.3'
```

### About ###

![Screenshot_2016-02-27-19-14-46.png](https://bitbucket.org/repo/Rqkqak/images/59871380-Screenshot_2016-02-27-19-14-46.png)

After spending hours fighting the system trying to customize text colors or background colors just to hit a wall placed by the platform i felt it would be better to come up with a fully customize-able Dialog for android.

This dialog accept a layout with predefined id's and use it as a window layout when displaying the dialog. it support all the currently available features of the native dialog but without the headache that comes with trying to customize it.

### Usage ###

* Optionally create a layout for the dialog, please note the required id's for the layout (positioning and view types are mostly unimportant and the system will notify you if you're missing any or used wrong type for type dependent items such as the title), here is an example dialog window layout which is used by default by the StyleableDialog:


```
#!xml

<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
              ...
              android:minWidth="300dp">

    <LinearLayout
        ...
        >

        <ImageView
            android:id="@+id/dialog_icon"
            ...
            />

        <TextView
            android:id="@+id/dialog_title"
            ...
            />

    </LinearLayout>

    <View
        android:id="@+id/dialog_title_divider"
        ...
        />

    <TextView
        android:id="@+id/dialog_message"
        ...
        />

    <FrameLayout
        android:id="@+id/dialog_content_view"
        ...
        >

        <!-- Customized view -->

    </FrameLayout>

    <View
        android:id="@+id/dialog_button_bar_divider"
        ...
        />

    <LinearLayout
        android:id="@+id/dialog_button_bar"
        ...
        >

        <!-- Buttons -->

    </LinearLayout>

</LinearLayout>
```

* Create a StyleableDialog object

```
#!java

StyleableDialog dialog = new StyleableDialog(MainActivity.this);
```
* Set dialog attributes depending on your needs (if its not possible via code you can always use custom layout):

```
#!java

dialog.setWindowView(R.layout.dialog_test_layout); //optionally provide custom layout
                dialog.setTitle("Welcome to my app");
                dialog.setMessage("Glad to see you !");
```

* Add buttons to your dialog (displayed in the order they are added):

```
#!java

dialog.addButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ...
                    }
                });
```
* And just show the dialog when you need it:

```
#!java

dialog.show();
```

* If you want even more customization you can provide custom button layout:

```
#!java

dialog.setButtonsLayout(R.layout.dialog_button_layout);
```

```
#!xml

<?xml version="1.0" encoding="utf-8"?>
<TextView
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/dialog_button"
    android:layout_weight="1"
    android:layout_width="0dp"
    android:layout_height="wrap_content"
    ...
    />
```

------------------------------

# ExpandableLinearLayout#

**Current standalone version:**

```
#!java

compile 'com.lirancohen.designtoolbox:designtoolbox-expandable-linear-layout:0.0.8'
```

### About ###

![Untitled-1.gif](https://bitbucket.org/repo/Rqkqak/images/1764740767-Untitled-1.gif)

It is not uncommon for design to contain some piece of content nested inside another in a header-content type of way. While a lot of widgets out there attempt to give this functionality many of them simply only work halfway.

This layout will wrap whatever content you give it in an expanding view and provide a header which toggle the collapse/expand state. The uniqueness of this widget is its ability to be nested multiple ways (though android guideline state you should avoid doing so too much).

### Usage ###

* Create a layout containing a ExpandableLinearLayout
* Put your content inside the view's xml (or add it dynamically in java)
* The layout's header will change its activated state when collapsed/expanded so you can create some toggle marker
* if you need to use getChildAt() or getChildCount() methods use the public api to interact with the expanding part: getExpandableChildAt() and getExpandableChildCount()
* You can set your own header or manipulate the existing one by getting its instance using: getHeaderView()

### Custom XML attributes ###

* initialState - [enum] set the initial collapse/expand state your view will take.
* headerResource - [resource] a header layout resource
* stateChangeDuration - [int] set how long expand/collapse animation should take in milliseconds
* itemTitle - [resource/string] set a title to be set to the header view if it contain a TextView in the main child